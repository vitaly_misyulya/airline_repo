Parser type: SAX
Airline: Fly Emirates
Planes:
 1. Passenger Airplane    {id = 1001 manufacturer = Airbus      model = A300B4         emptyWeight =  88500 maxRange =  6670 fuelConsumption =  9.425 capacity = 345
 2. Passenger Airplane    {id = 1002 manufacturer = Airbus      model = A340-200       emptyWeight = 130000 maxRange = 15000 fuelConsumption =  5.581 capacity = 375
 3. Passenger Airplane    {id = 1003 manufacturer = Boeing      model = 737-800        emptyWeight =  41413 maxRange =  5665 fuelConsumption =  4.591 capacity = 220
 4. Passenger Airplane    {id = 1004 manufacturer = Boeing      model = 777-300ER      emptyWeight = 167800 maxRange = 14490 fuelConsumption = 12.511 capacity = 550
 5. Cargo Airplane        {id = 2001 manufacturer = Airbus      model = A300-600ST     emptyWeight =  86000 maxRange =  4600 fuelConsumption = 14.713 cargoMass =  47000 cargoVolume =  1410
 6. Cargo Airplane        {id = 2002 manufacturer = Antonov     model = An-225         emptyWeight =  86000 maxRange = 15400 fuelConsumption = 24.351 cargoMass = 250000 cargoVolume =  1300
 7. Firefighting Airplane {id = 3001 manufacturer = Bombardier  model = 415            emptyWeight =  12880 maxRange =  2443 fuelConsumption =  2.379 waterTankVolume =   6140
 8. Passenger Airplane    {id = 1005 manufacturer = Sukhoi      model = Superjet 100   emptyWeight =  24250 maxRange =  4578 fuelConsumption =  2.929 capacity = 108
 9. Passenger Airplane    {id = 1006 manufacturer = Embaraer    model = E-170          emptyWeight =  21140 maxRange =  3100 fuelConsumption =  2.990 capacity =  78
10. Passenger Airplane    {id = 1007 manufacturer = Boeing      model = 747-8I         emptyWeight = 213200 maxRange = 14800 fuelConsumption =  8.706 capacity = 605
11. Passenger Airplane    {id = 1008 manufacturer = Tupolev     model = Tu-334         emptyWeight =  30050 maxRange =  3150 fuelConsumption =  3.066 capacity = 102
12. Cargo Airplane        {id = 2003 manufacturer = Antonov     model = An-124 Ruslan  emptyWeight = 175000 maxRange =  5200 fuelConsumption = 51.195 cargoMass = 230000 cargoVolume =  1160
13. Cargo Airplane        {id = 2004 manufacturer = Airbus      model = A380F          emptyWeight = 252000 maxRange = 10400 fuelConsumption = 40.981 cargoMass = 152000 cargoVolume =  1134
14. Cargo Airplane        {id = 2005 manufacturer = Boeing      model = 777 Freighter  emptyWeight = 144400 maxRange =  9070 fuelConsumption = 19.977 cargoMass = 103000 cargoVolume =   653
15. Firefighting Airplane {id = 3002 manufacturer = Beriev      model = Be-200         emptyWeight =  27600 maxRange =  2100 fuelConsumption =  2.245 waterTankVolume =  12000
Statistics:
Overall capacity: 2383
Overall cargo capacity: 782000

Planes sorted by maxRange:
 1. Firefighting Airplane {id = 3002 manufacturer = Beriev      model = Be-200         emptyWeight =  27600 maxRange =  2100 fuelConsumption =  2.245 waterTankVolume =  12000
 2. Firefighting Airplane {id = 3001 manufacturer = Bombardier  model = 415            emptyWeight =  12880 maxRange =  2443 fuelConsumption =  2.379 waterTankVolume =   6140
 3. Passenger Airplane    {id = 1006 manufacturer = Embaraer    model = E-170          emptyWeight =  21140 maxRange =  3100 fuelConsumption =  2.990 capacity =  78
 4. Passenger Airplane    {id = 1008 manufacturer = Tupolev     model = Tu-334         emptyWeight =  30050 maxRange =  3150 fuelConsumption =  3.066 capacity = 102
 5. Passenger Airplane    {id = 1005 manufacturer = Sukhoi      model = Superjet 100   emptyWeight =  24250 maxRange =  4578 fuelConsumption =  2.929 capacity = 108
 6. Cargo Airplane        {id = 2001 manufacturer = Airbus      model = A300-600ST     emptyWeight =  86000 maxRange =  4600 fuelConsumption = 14.713 cargoMass =  47000 cargoVolume =  1410
 7. Cargo Airplane        {id = 2003 manufacturer = Antonov     model = An-124 Ruslan  emptyWeight = 175000 maxRange =  5200 fuelConsumption = 51.195 cargoMass = 230000 cargoVolume =  1160
 8. Passenger Airplane    {id = 1003 manufacturer = Boeing      model = 737-800        emptyWeight =  41413 maxRange =  5665 fuelConsumption =  4.591 capacity = 220
 9. Passenger Airplane    {id = 1001 manufacturer = Airbus      model = A300B4         emptyWeight =  88500 maxRange =  6670 fuelConsumption =  9.425 capacity = 345
10. Cargo Airplane        {id = 2005 manufacturer = Boeing      model = 777 Freighter  emptyWeight = 144400 maxRange =  9070 fuelConsumption = 19.977 cargoMass = 103000 cargoVolume =   653
11. Cargo Airplane        {id = 2004 manufacturer = Airbus      model = A380F          emptyWeight = 252000 maxRange = 10400 fuelConsumption = 40.981 cargoMass = 152000 cargoVolume =  1134
12. Passenger Airplane    {id = 1004 manufacturer = Boeing      model = 777-300ER      emptyWeight = 167800 maxRange = 14490 fuelConsumption = 12.511 capacity = 550
13. Passenger Airplane    {id = 1007 manufacturer = Boeing      model = 747-8I         emptyWeight = 213200 maxRange = 14800 fuelConsumption =  8.706 capacity = 605
14. Passenger Airplane    {id = 1002 manufacturer = Airbus      model = A340-200       emptyWeight = 130000 maxRange = 15000 fuelConsumption =  5.581 capacity = 375
15. Cargo Airplane        {id = 2002 manufacturer = Antonov     model = An-225         emptyWeight =  86000 maxRange = 15400 fuelConsumption = 24.351 cargoMass = 250000 cargoVolume =  1300
Planes with fuel consumption in range [5.0, 15.0]:
 1. Passenger Airplane    {id = 1001 manufacturer = Airbus      model = A300B4         emptyWeight =  88500 maxRange =  6670 fuelConsumption =  9.425 capacity = 345
 2. Passenger Airplane    {id = 1002 manufacturer = Airbus      model = A340-200       emptyWeight = 130000 maxRange = 15000 fuelConsumption =  5.581 capacity = 375
 3. Passenger Airplane    {id = 1004 manufacturer = Boeing      model = 777-300ER      emptyWeight = 167800 maxRange = 14490 fuelConsumption = 12.511 capacity = 550
 4. Cargo Airplane        {id = 2001 manufacturer = Airbus      model = A300-600ST     emptyWeight =  86000 maxRange =  4600 fuelConsumption = 14.713 cargoMass =  47000 cargoVolume =  1410
 5. Passenger Airplane    {id = 1007 manufacturer = Boeing      model = 747-8I         emptyWeight = 213200 maxRange = 14800 fuelConsumption =  8.706 capacity = 605