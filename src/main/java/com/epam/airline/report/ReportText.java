package com.epam.airline.report;

import com.epam.airline.entity.AbstractAirplane;
import com.epam.airline.entity.Airline;
import com.epam.airline.logic.AirlineStatistics;
import com.epam.airline.util.ResourceManager;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.List;


public class ReportText {

    private static final Logger LOG = Logger.getLogger(ReportText.class);
    private static final ResourceManager RESOURCE_MANAGER = ResourceManager.INSTANCE;

    private StringBuilder report = new StringBuilder();

    public void appendString(String str) {
        report.append(str);
    }

    public void appendAirline(Airline airline) {
        report.append(String.format(RESOURCE_MANAGER.getString("report.format1"),
                RESOURCE_MANAGER.getString("report.airline"),
                airline.getName(),
                RESOURCE_MANAGER.getString("report.planes")));
        appendAirplaneList(airline.getPlanes());

        report.append(String.format(RESOURCE_MANAGER.getString("report.format2"),
                RESOURCE_MANAGER.getString("report.statistics"),
                RESOURCE_MANAGER.getString("report.overall-capacity"),
                AirlineStatistics.calcOverallCapacity(airline),
                RESOURCE_MANAGER.getString("report.overall-cargo-capacity"),
                AirlineStatistics.calcOverallCargoMass(airline)));
    }

    public void appendAirplaneList(List<AbstractAirplane> planes) {
        int i = 1;
        for (AbstractAirplane airplane : planes) {
            report.append(String.format(ResourceManager.INSTANCE.getString("report.plane-format"),
                    i, airplane.toString()));
            i++;
        }
    }

    public void clearReport() {
        report = new StringBuilder();
    }

    public boolean writeReportToFile(String filePath) {
        boolean isSuccessfulWrite = true;
        File file = new File(filePath);
        try (PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(file)))) {
            pw.print(report);
        } catch (IOException e) {
            LOG.error("Can't write report to file: " + file.getAbsolutePath());
            isSuccessfulWrite = false;
        }
        return isSuccessfulWrite;
    }
}
