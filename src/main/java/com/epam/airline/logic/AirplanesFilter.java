package com.epam.airline.logic;

import com.epam.airline.entity.AbstractAirplane;
import com.epam.airline.entity.Airline;
import com.epam.airline.entity.AirplaneManufacturer;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class AirplanesFilter {

    /*
     * Initialize predicate with dummy test() function that always return true
     * so there is no necessity to check whether predicate is null before
     * appending another predicate
     */
    private Predicate<AbstractAirplane> predicate = p -> true;
    private Airline airline;

    public AirplanesFilter(Airline airline) {
        this.airline = airline;
    }

    public List<AbstractAirplane> filter() {
        return airline.getPlanes().stream().filter(predicate).collect(Collectors.<AbstractAirplane>toList());
    }

    public AirplanesFilter addManufacturer(AirplaneManufacturer manufacturer) {
        predicate = predicate.and(isManufacturerMatch(manufacturer));
        return this;
    }

    public AirplanesFilter addMinEmptyWeight(int minEmptyWeight) {
        predicate = predicate.and(isEmptyWeightGreaterThan(minEmptyWeight));
        return this;
    }

    public AirplanesFilter addMaxEmptyWeight(int maxEmptyWeight) {
        predicate = predicate.and(isEmptyWeightLessThan(maxEmptyWeight));
        return this;
    }

    public AirplanesFilter addMinMaxRange(int minMaxRange) {
        predicate = predicate.and(isMaxRangeGreaterThan(minMaxRange));
        return this;
    }

    public AirplanesFilter addMaxMaxRange(int maxMaxRange) {
        predicate = predicate.and(isMaxRangeLessThan(maxMaxRange));
        return this;
    }

    public AirplanesFilter addMinFuelConsumption(double minFuelConsumption) {
        predicate = predicate.and(isFuelConsumptionGreaterThan(minFuelConsumption));
        return this;
    }

    public AirplanesFilter addMaxFuelConsumption(double maxFuelConsumption) {
        predicate = predicate.and(isFuelConsumptionLessThan(maxFuelConsumption));
        return this;
    }

    private static Predicate<AbstractAirplane> isManufacturerMatch(AirplaneManufacturer manufacturer) {
        return p -> p.getManufacturer() == manufacturer;
    }

    private static Predicate<AbstractAirplane> isEmptyWeightLessThan(double maxEmptyWeight) {
        return a -> a.getEmptyWeight() <= maxEmptyWeight;
    }

    private static Predicate<AbstractAirplane> isEmptyWeightGreaterThan(double minEmptyWeight) {
        return a -> a.getEmptyWeight() >= minEmptyWeight;
    }

    private static Predicate<AbstractAirplane> isMaxRangeLessThan(double maxMaxRange) {
        return a -> a.getMaxRange() <= maxMaxRange;
    }

    private static Predicate<AbstractAirplane> isMaxRangeGreaterThan(double minMaxRange) {
        return a -> a.getMaxRange() >= minMaxRange;
    }

    private static Predicate<AbstractAirplane> isFuelConsumptionLessThan(double maxFuelConsumption) {
        return a -> a.getFuelConsumption() <= maxFuelConsumption;
    }

    private static Predicate<AbstractAirplane> isFuelConsumptionGreaterThan(double minFuelConsumption) {
        return a -> a.getFuelConsumption() >= minFuelConsumption;
    }
}