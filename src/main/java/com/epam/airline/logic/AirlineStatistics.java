package com.epam.airline.logic;

import com.epam.airline.entity.Airline;
import com.epam.airline.entity.CargoAirplane;
import com.epam.airline.entity.PassengerAirplane;


public class AirlineStatistics {

    public static int calcOverallCapacity(Airline airline) {
        return airline.getPlanes()
                .stream()
                .filter(p -> p instanceof PassengerAirplane)
                .mapToInt(p -> ((PassengerAirplane) p).getCapacity())
                .sum();
    }

    public static int calcOverallCargoMass(Airline airline) {
        return airline.getPlanes()
                .stream()
                .filter(p -> p instanceof CargoAirplane)
                .mapToInt(p -> ((CargoAirplane) p).getCargoMass())
                .sum();
    }
}
