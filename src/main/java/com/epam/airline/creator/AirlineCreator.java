package com.epam.airline.creator;

import com.epam.airline.entity.Airline;
import com.epam.airline.exception.TechnicalException;
import com.epam.airline.parser.AbstractAirlineBuilder;
import com.epam.airline.parser.AirlineBuilderFactory;
import com.epam.airline.parser.ParserType;


public class AirlineCreator {

    /**
     * Creates airline parsing xml file with parser defined by parserType
     *
     * @param parserType type of parser to parse xml
     * @param filePath   path to xml file
     * @return Airline that was parsed from xml file
     */
    public static Airline createAirlineFromXml(ParserType parserType, String filePath) throws TechnicalException {
        AbstractAirlineBuilder builder = AirlineBuilderFactory.createAirlineBuilder(parserType);
        builder.buildAirline(filePath);
        return builder.getAirline();
    }
}
