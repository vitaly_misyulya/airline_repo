package com.epam.airline.run;

import com.epam.airline.creator.AirlineCreator;
import com.epam.airline.entity.AbstractAirplane;
import com.epam.airline.entity.Airline;
import com.epam.airline.exception.TechnicalException;
import com.epam.airline.logic.AirplanesFilter;
import com.epam.airline.parser.ParserType;
import com.epam.airline.report.ReportText;
import com.epam.airline.util.ResourceManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class AirlineRunner {

    private static final Logger LOG = Logger.getLogger(AirlineRunner.class);

    private static final String REPORT_FILE_PATH = ResourceManager.INSTANCE.getString("report.file-path");
    private static final String REPORT_FILE_EXT = ResourceManager.INSTANCE.getString("report.file-extension");
    private static final String DATE_TIME_FORMAT = ResourceManager.INSTANCE.getString("report.date-time-format");
    private static final String XML_FILE_PATH = ResourceManager.INSTANCE.getString("xml-file-path");

    static {
        new DOMConfigurator().doConfigure("config/log4j.xml", LogManager.getLoggerRepository());
//        PropertyConfigurator.configure("config/log4j.properties");
    }

    public static void main(String[] args) {

        ResourceManager resourceManager = ResourceManager.INSTANCE;

        ReportText reportText = new ReportText();

        ParserType parserType = ParserType.STAX;

        Airline airline = null;

        try {
            airline = AirlineCreator.createAirlineFromXml(parserType, XML_FILE_PATH);
        } catch (TechnicalException e) {
            LOG.fatal(e.getMessage());
            LOG.info("Shutting down");
            return;
        }

        reportText.appendString(resourceManager.getString("report.parser-type") + parserType);
        reportText.appendAirline(airline);

        ArrayList<AbstractAirplane> planes = new ArrayList<>(airline.getPlanes());
        Collections.sort(planes, AbstractAirplane.maxRangeComparator());
        planes.sort(AbstractAirplane.maxRangeComparator());

        reportText.appendString(resourceManager.getString("report.planes-sorted"));
        reportText.appendAirplaneList(planes);

        double minFuelConsumption = 5.;
        double maxFuelConsumption = 15.;

        List<AbstractAirplane> filtered = new AirplanesFilter(airline)
                .addMinFuelConsumption(minFuelConsumption)
                .addMaxFuelConsumption(maxFuelConsumption)
                .filter();

        reportText.appendString(resourceManager.getString("report.planes-fuel_consumption") + minFuelConsumption + ", " + maxFuelConsumption + "]:");
        reportText.appendAirplaneList(filtered);

        LocalDateTime ldt = LocalDateTime.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);
        reportText.writeReportToFile(REPORT_FILE_PATH + ldt.format(dateTimeFormatter) + REPORT_FILE_EXT);
    }
}
