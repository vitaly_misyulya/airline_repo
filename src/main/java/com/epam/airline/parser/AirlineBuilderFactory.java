package com.epam.airline.parser;


import com.epam.airline.exception.TechnicalException;

public class AirlineBuilderFactory {

    public static AbstractAirlineBuilder createAirlineBuilder(ParserType parserType) throws TechnicalException {
        switch (parserType) {
            case DOM:
                return new AirlineDOMBuilder();
            case SAX:
                return new AirlineSAXBuilder();
            case STAX:
                return new AirlineStaxBuilder();
            default:
                throw new EnumConstantNotPresentException(parserType.getDeclaringClass(), parserType.name());
        }
    }
}
