package com.epam.airline.parser;

import com.epam.airline.entity.Airline;
import com.epam.airline.exception.TechnicalException;

public abstract class AbstractAirlineBuilder {

    protected Airline airline;

    public AbstractAirlineBuilder() {
        this.airline = new Airline();
    }

    public Airline getAirline() {
        return airline;
    }

    abstract public void buildAirline(String fileName) throws TechnicalException;
}
