package com.epam.airline.parser;


public enum ParserType {
    SAX, DOM, STAX;
}
