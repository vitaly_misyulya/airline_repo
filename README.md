Task #9. Airline

1. Define hierarchy of airplanes.
2. Create airline: parse XML using SAX, DOM or StAX parser.
3. Calculate overall capacity and payload.
4. Sort airline's planes by range.
5. Find plane in airline that fits in given range of fuel consumption parameters.